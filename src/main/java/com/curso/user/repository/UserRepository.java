package com.curso.user.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.curso.user.model.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{

	List<User> findAll();
	User save(User user);
	List<User> findByIdIn(List<Long> ids);
}
