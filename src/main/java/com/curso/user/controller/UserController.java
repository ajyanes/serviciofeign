package com.curso.user.controller;

import com.curso.user.model.entity.RequestIdUserDto;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.curso.user.model.entity.UserDto;
import com.curso.user.service.IUserService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping("/user")
public class UserController {

  @Autowired
  IUserService userService;

  @GetMapping("/listarUser")
  public List<UserDto> listUsuarios() {

    return userService.consultar();
  }

  @PostMapping("/guardarUser")
  public UserDto guardar(@RequestBody final UserDto userDto) {
    return userService.guardar(userDto);
  }

  @PostMapping("/listUserById")
  public List<UserDto> listUsuariosByListId(@RequestBody final RequestIdUserDto requestIdUserDto) {

    return userService.listUserByIds(requestIdUserDto);
  }

}
