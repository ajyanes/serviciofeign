package com.curso.user.service;

import com.curso.user.model.entity.RequestIdUserDto;
import java.util.List;

import com.curso.user.model.entity.UserDto;

public interface IUserService {

	List<UserDto> consultar();
  UserDto guardar(UserDto userDto);
  List<UserDto> listUserByIds(RequestIdUserDto requestIdUserDto);

}
