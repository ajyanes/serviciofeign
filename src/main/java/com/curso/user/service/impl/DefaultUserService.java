package com.curso.user.service.impl;

import com.curso.user.model.entity.RequestIdUserDto;
import java.util.ArrayList;
import java.util.List;

import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.curso.user.mapper.UserMapper;
import com.curso.user.model.entity.User;
import com.curso.user.model.entity.UserDto;
import com.curso.user.repository.UserRepository;
import com.curso.user.service.IUserService;

@Service
public class DefaultUserService implements IUserService {

  @Autowired
  UserRepository userRepository;

  UserMapper userMapper = Mappers.getMapper(UserMapper.class);

  @Override
  public List<UserDto> consultar() {
    List<User> users = userRepository.findAll();
    List<UserDto> usersDto = new ArrayList<>();

    for (User u : users) {
      UserDto uDto = userMapper.userToUserDto(u);
      usersDto.add(uDto);
    }
    return usersDto;
  }

  @Override
  public UserDto guardar(UserDto userDto) {
    User user = userMapper.userDtoToUser(userDto);
    user = userRepository.save(user);

    UserDto udto = userMapper.userToUserDto(user);
    return udto;
  }

  @Override
  public List<UserDto> listUserByIds(RequestIdUserDto requestIdUserDto) {
    List<User> users = userRepository.findByIdIn(requestIdUserDto.getListFind());
    List<UserDto> usersDto = new ArrayList<>();
    for (User u : users) {
      UserDto uDto = userMapper.userToUserDto(u);
      usersDto.add(uDto);
    }
    return usersDto;

  }

}
