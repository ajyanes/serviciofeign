package com.curso.user.mapper;

import org.mapstruct.Mapper;

import com.curso.user.model.entity.User;
import com.curso.user.model.entity.UserDto;


@Mapper
public interface UserMapper {
    
    UserDto userToUserDto(User user);    
        
    User userDtoToUser(UserDto userDto);
}
